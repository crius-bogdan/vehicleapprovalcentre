const init_data = {
  // Все марки авто
  brands: [],
  /*
    В интерфейсе есть специфическое поле поиска
    в котором по ключевому слову ищутся все модели и марки авто
  */
  all_models: [],
  // Все типы кузова
  body_types: [
    'Trucks',
    'SUV',
    'Sedan',
    'Hatchback',
    'Coupe',
    'Convertiable',
    'VAN',
  ],
  /*
    При первом заходе показываются рекомендованные авто
    (в админ панели должно быть поле как checkbox
    с помощью которого админ отмечает рекомендованны это автомобиль или нет)
    =====
    Если нет рекомендованных автомобилей показываются последние
    =====
    И так с каждыми запросами сначала в масиве должны показываться рекомендованные
    авто потом остальные. Если рекомендованных авто нет то показываются последние
   */
  cars: []
};

const get_models = {
  /*
    Все модели авто
    мне надо будет их получать когда человек выберет марку авто
 */
  models: []
}
/*
  Параметры могут быть выбраны все или несколько
*/
const test_request_params = {
  // brand будет Array UPDATES
  brand: Array,
  // model будет Array UPDATES
  model: Array,
  // UPDATES будет Array
  body_type: Array,
  transmission_type: String,
  price: {
    from: [String, Number], // String or number
    to: [String, Number], // String or number
  },
  year: {
    from: [String, Number], // String or number
    to: [String, Number], // String or number
  },
  kilometers: Number
}

const car_card = {
  id: Number,
  /*
  * Рекомендовано админом (true, false)
  * */
  is_recommended: Boolean,
  /*
    Масив из 4 абсолютных ссылок изображений для картчки машины
    которые выбрал админ
 */
  images: [],
  /*
    Название и модель машины (в общеи говоря тайтл который пропишет админ)
 */
  title: String,
  /*
    Цена автомобиля
 */
  price: Number,
  /*
    Год выпуска
 */
  year: 'Format YYYY-MM-DD',
  /*
    Тип кузова
  */
  body_type: String,
  /*
  * Тип трансмисии
  * */
  transmission_type: String,
  /*
  * Пробег
  * */
  kilometers: Number
}

const request_one_car = {
  id: Number,
  /*
  * Рекомендовано админом (true, false)
  * */
  is_recommended: Boolean,
  /*
  * Название и модель машины (в общеи говоря тайтл который пропишет админ)
  * */
  title: String,
  /*
  * Настройки для 360 изображений
  * */
  three_sixty: {
    // Абсолютная ссылка на риректорию со всеми изображениями
    directory: String,
    /*
    * Все иимена у изображений должны быть в формате 'slug_(image_number)'
    * отсчет начинается с 0
    * по итогу должно получиться что то типа:
    * '2304982sdfsdk098_1.jpg'
    * */
    slug: String,
    /*
    * Количесвто изображений
    * */
    total_images_count: Number,
  },

  three_sixty_preview: String,
  /*
  * Большая картинка для того чтобы показать салон UPDATES
  * */
  three_sixty_interior: String,
  /*
  * Изображения для слайдера (абсолютные ссылки) c превьюшками UPDATES
  * */
  slider_images: [],
  /*
  * Цена (масив показан просто для примера чтобы ты понимал что тип не важен)
  * */
  price: [Number, String],
  /*
  * Тип кузова
  * */
  body_type: String,
  /*
  * Год
  * */
  year: 'Format YYYY-MM-DD',
  /*
  * Трансмисия
  * */
  transmission_type: String,
  /*
  * Пробег (масив показан просто для примера чтобы ты понимал что тип не важен)
  * */
  kilometers: [String,Number],
  /*
  * Больше инфы
  * тут может быть неограниченное количесво полей
  * */
  details: {
    /*
    * Пример
    * https://www.dropbox.com/s/9qnce50954oo12o/%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202020-06-17%2003.35.27.png?dl=0
    * */
    'Ключ это название спецефической детали': 'значение соответственно контент этой детали (макс 25 символов)'
  },
  /*
  * Описане макс 750 символов
  * */
  description: String
}
