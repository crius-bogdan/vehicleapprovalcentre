export default async ({ store, redirect, route: { params: { id } } }) => {
  console.log('lol')
  const result = await store.dispatch('car/get_car', id)
  if(!result) redirect('/inventory')
}
