export default async ({ store, redirect }) => {
  const is_passed = await store.dispatch('inventory/check_ip')
  if(!is_passed) redirect('/')
}
