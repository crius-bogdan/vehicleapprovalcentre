export default async ({ store, redirect, route: { query } }) => {
  // redirect('/')
  let string_query = '';
  if(Object.keys(query).length > 0) {
    for(let key in query) {
      key = key.replace(/\[|\]/g, '');
      if((key === 'body' || key === 'transmission') && query[key + '[]'].length > 0 ) {
        for(let item of query[key + '[]']) {
          string_query += `${key}[]=${item}&`;
        }
        continue;
      }
      if(key) {
        string_query += `${key}=${query[key]}&`;
      }
    }
  }
  const get_date = await store.dispatch('inventory/get_first_data', string_query)
  if(!get_date) redirect('/')
}
