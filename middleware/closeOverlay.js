import objectValue from '~/utils/objectValue';

export default ({store, redirect}) => {
  const overlay = objectValue(store, 'state.overlay.overlay');
  if (overlay) {
    store.commit('overlay/updateField', {path: 'overlay', value: null});
  }
}
