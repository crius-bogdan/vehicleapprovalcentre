import './env-config';
const imageminMozjpeg = require('imagemin-mozjpeg')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const isDev = process.env.NODE_ENV !== 'production'
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#7481FF', height: '5px' },
  // loading: '~/components/loader/Nuxt.vue',
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {src: '~/plugins/vue-simple-svg.js'},
    {src: '~/plugins/vue-lazyload.js', mode: 'client'},
    {src: '~/plugins/aos.js', mode: 'client'},
    { src: '~/plugins/vue-clipboard2.js', mode: 'client' },
    // { src: '~/plugins/vue-zoomer.js', mode: 'client' },
    { src: '~/directives', mode: 'client' },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/dayjs',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    // proxy: true,
    credentials: true,
    headers: {
      common: {
        'Content-Type': 'application/x-www-form-urlencoded',
        // 'Host': 'https://api.vehicleapprovalcentre.com',
        // 'Origin': 'https://front.vehicleapprovalcentre.com'
      }
    }
  },
  router: {
    prefetchLinks: false
  },
  /*
  ** Build configuration
  */
  //_nuxt-imports
  build: {
    styleResources: {
      scss : './assets/scss/_nuxt-imports.scss'
    },
    splitChunks: {
      layouts: true,
      pages: true,
      commons: true
    },
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      const ORIGINAL_TEST = '/\\.(png|jpe?g|gif|svg|webp)$/i'
      const imageMinPlugin = new ImageminPlugin({
        pngquant: {
          quality: '5-30',
          speed: 7,
          strip: true
        },
        jpegtran: {
          progressive: true
        },
        gifsicle: {
          interlaced: true
        },
        plugins: [
          imageminMozjpeg({
            quality: 70,
            progressive: true
          })

        ]
      })
      if (!ctx.isDev) config.plugins.push(imageMinPlugin)
      config.module.rules.forEach(rule => {
        if (rule.test.toString() === ORIGINAL_TEST) {
          rule.test = /\.(png|jpe?g|gif|webp)$/i
          rule.use = [
            {
              loader: 'url-loader',
              options: {
                limit: 1000,
                name: ctx.isDev ? '[path][name].[ext]' : 'img/[contenthash:7].[ext]'
              }
            }
          ]
        }
      })
    }
  }
}
