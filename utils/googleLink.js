export default (token) => {
  return `https://cors-anywhere.herokuapp.com/https://script.google.com/macros/s/${token}/exec`
}
