const All_data = {
  Home_page: {
    metadata: {
      title: 'Page Title',
      keywords: 'page Keywords',
      description: 'page description'
    },
    header: {
      title: 'The easiest way to buy a car in Canada'
    },
    how_it_working: {
      title: 'How does it work?',
      subtitle: 'From your Phone to your Home',
      text: 'We make it easy for you to buy your dream car from your phone without ever having to step into a dealership! We deliver your vehicle to your home for you to test drive before you sign anything.'
    },
    steps_section: {
      title: 'Your perfect car in',
      steps: [
        {
          id: 3242,
          title: 'First Contact',
          text: 'Tell us what your dream vehicle is.'
        },
        {
          id: 3432,
          title: 'Vehicle Selection',
          text: 'One of our Qualified Agents sends you custom pictures and videos of the car you are interested in.'
        },
        {
          id: 3432,
          title: 'Secure Financing',
          text: 'You work with a dedicated Qualified Agent to secure financing on your terms.'
        },
        {
          id: 3432,
          title: 'Vehicle Delivery',
          text: 'We deliver your car to your home or office for you to test drive.'
        },
        {
          id: 3432,
          title: 'Getting Vehicle',
          text: 'If you love the vehicle, you sign the paperwork and keep the car.'
        },
      ]
    },
    advantages_section: {
      title: 'Advantages of working with us',
      adv_items: [
        {
          id: 234,
          icon: 'url to svg icon',
          title: 'Vehicle Delivery',
          text: 'We deliver your vehicle directly to your door. You don’t sign anything until you test drive the vehicle. There is never any obligation to buy beforehand.'
        },
        {
          id: 234,
          icon: 'url to svg icon',
          title: 'Auto Financing',
          text: 'We partner with the largest lenders in Canada to offer the strongest financing options in the industry—including rates as low as 4%'
        },
        {
          id: 234,
          icon: 'url to svg icon',
          title: 'Huge Inventory',
          text: 'We have access to thousands of high-quality vehicles and offer them to you at wholesale prices. Your dream car is waiting for you'
        },
      ]
    },
    cta_inventory_section: {
      title: 'Сhoose your dream vehicle in our catalog',
      text: 'In our catalogue there are many models of vehicles. Choose your car to your liking.'
    },
    cta_stats: {
      title: 'Happy customers',
      text: 'Since 2017, we have helped thousands of Canadians get their dream vehicles from the comfort of their homes.',
      subtitle: 'What are you waiting?  Get started right now!',
      block_items: [
        {
          id: 342,
          title: '7,988+',
          text: 'Sold Cars'
        },
        {
          id: 342,
          title: '16,974+',
          text: 'Vehicles To Choose From'
        },
        {
          id: 342,
          title: '123',
          text: 'Experts Across Canada'
        },
      ],
    },
    reviews_section: {
      title: 'What our client say about us!',
      reviews_items: [
        {
          id: 23423,
          image: 'url to image',
          name: 'Laura Chaisson',
          text: 'In irure et ipsum eiusmod adipisicing id elit cillum proident. Adipisicing ullamco nisi veniam occaecat id excepteur ad non. Sunt occaecat elit ullamco commodo exercitation. Anim mollit irure Lorem proident veniam excepteur non irure Lorem. Lorem esse sint in nisi id esse.'
        }
      ]
    },
    calculator_section: {
      title: 'Let’s figure out how much you can afford',
      text: 'Before you start shopping, let’s figure out how much you can afford. Move the sliders to see how the loan duration and the total loan amount affect your monthly payments.'
    },
  },
  Calculator_page: {
    metadata: {
      title: 'Use our free calculator!',
      description: 'etc',
    }
  },
  Videos_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    videos_section: [
      {
        id: 23423,
        preview: 'url to image',
        title: 'VAC\'s Vehicle Shoot #10: 2018 Chevy Cruze Hatchback',
        y_url: 'youtube video url'
      },
    ]
  },
  About_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    mission_section: {
      title: 'Our mission',
      text: 'Our mission at VAC is to help you find the perfect car at the perfect price and with the perfect auto loan. We make buying a car simple. You can complete the entire process from home—we’ll even deliver the car to you!'
    },
    dream_section: {
      title: 'Get your dream car',
      text: 'We founded VAC because we hated how complicated it was to buy a car. You only have so much free time in your life. You don’t want to waste it going to different car dealerships every weekend for weeks and months trying to track down the right car.\n We also hated how choosing a car and figuring out the financing for the vehicle were treated as totally separate processes! Instead of dealing with a salesperson, then being handed off to a closer, and then shuffled over to a finance manager, we wanted to create a way for people to buy a car and only deal with a single person for the entire process.',
      adv_items: [
        {
          id: 2343,
          title: 'Quickly',
          text: 'You do not spend a lot of your time going to car dealerships'
        },
        {
          id: 2343,
          title: 'Simply',
          text: 'You can order your dream car just by talking to our specialist by phone'
        },
        {
          id: 2343,
          title: 'Conveniently',
          text: 'We will deliver the car directly to the doorstep of your home or office'
        },
      ]
    },
    adv_section: {
      title: 'VAC is convenience and transparency',
      text: 'We want to make simple for you to find the car you want. We also want to make sure you are 100% comfortable with the terms of the conditions of your auto loan.\n Buying a car has never been easier. You pick out the car you want on our website. You work with one of our Qualified Agents to secure financing. Then we bring the car to you—all before you sign anything. You test drive the vehicle, and if you like it, you sign the loan documents right there and the car is yours.'
    },
    adv_best_choice_section: {
      title: 'Get a car at the best prices and conditions',
      content: 'Not only is buying a car with VAC easy, it’s also the best way to save money. We have partnerships with Canada’s largest lenders. That allows us to help you secure loans at the best interest rates available.\n Because convenience and transparency are our founding principles, we take the time to answer all of your questions about the vehicle and the auto loan. We will send you pictures and videos of any part of the car you like before you request a test drive.',
      subtitle: 'We think that VAC is the best way to buy a car in Canada. Try it for yourself and see how much time and money you can save.',
      sub_content: 'There are never any hidden fees or costs. We never ask you to sign anything until we have explained it clearly and you have had a chance to read it.',
    },
    agents_section: {
      title: 'Our qualified agents',
      text: 'The true secret to success of VAC isn’t our streamlined purchasing and financing processes. The real reason VAC is the easiest and best way to buy a car in Canada is because of our outstanding Qualified Agents. \n Our team provides you with the best customer experience in the automotive industry. You work with a dedicated representative throughout the process. They are able to answer any questions you have about any of our vehicles or the financing. Are you interested in seeing pictures or videos of a car? Your qualified agent will send them to you.',
      subtitle: 'Contact us today and speak with one of our qualified agents'
    },
    dynamic_team_section: {
      title: 'We\'re dynamic team of creative people with innovative mind',
      content_items: [
        {
          id: 324234,
          title: 'Our History',
          text: 'We founded VAC because we hated how complicated it was to buy a car. You only have so much free time in your life. You don’t want to waste it going to different car dealerships every weekend for weeks and months trying to track down the right car.'
        },
        {
          id: 45435,
          title: 'Our Goal',
          text: 'Buying a car has never been easier. You pick out the car you want on our website. You work with one of our Qualified Agents to secure financing. Then we bring the car to you—all before you sign anything. You test drive the vehicle, and if you like it, you sign the loan documents right there and the car is yours. '
        }
      ]
    },
    features_section: {
      title: 'Main features',
      text: 'We are your one stop shop. We believe that vehicle shopping should be a fun and painless process and with years of experience, we make it just that.',
      adv_blocks: [
        {
          id: 23423,
          title: '7,988+',
          text: 'Sold Cars'
        },
        {
          id: 23423,
          title: '123',
          text: 'Working Experts'
        },
        {
          id: 23423,
          title: '1560+',
          text: 'Happy Clients'
        },
      ]
    }
  },
  Loan_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    credit_policy_section: {
      title: 'Our credit policy',
      text: 'You want to secure a loan that works for your budget. Here at VAC we work with Canada’s largest lenders to help you find the best interest rates and terms for your auto loan.',
    },
    interested_credit_rates_section: {
      title: 'Interest rates and loan terms',
      interested_items: [
        {
          id: 3242323,
          icon: 'url to svg icon',
          title: 'What are my interest rate options?',
          text: 'We work to secure customized auto loans from Canada’s largest lenders. Your interest rate is critical for determining the total cost of your auto financing. Interest rates are based on many factors including the loan amount, your credit score, and your budget. We help you find the lowest interest rates available for your auto loan.',
        },
        {
          id: 3242323,
          icon: 'url to svg icon',
          title: 'How long will my loan  term be?',
          text: 'Our goal is to find the right car and the right loan for you. Because we have relationships with so many different lenders, we can help you find the right term length for your budget. The longer the loan is, often the lower the monthly payments will be—but the more expensive the loan will be overall. Most of the loans we secure for our clients are between 36 and 72 months.'
        },
      ]
    },
    adv_loan_section: {
      title: 'Benefits of getting an auto loan with VAC',
      text: 'For many people getting a reasonable auto loan is the best way for them to get a new car. However, the benefits of getting the right loan go beyond just being able to drive a nice vehicle.\n',
      bold_text: 'When you work with VAC, there are never any hidden costs or fees! We lay out all the terms and conditions so that you know exactly what details of the loan are before you sign anything.'
    },

  },
  Terms_conditions_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    terms_items: [
      {
        id: 23432,
        title: 'Please read these terms and conditions of use carefully before using this web site',
        text: 'Vehicle Approval Centre may, in its sole discretion and for any reason, modify, supplement or amend these Terms and Conditions without any notice or liability to you or any other person, by posting revised Terms and Conditions on the Vehicle Approval Centre website. Your continued use of the Vehicle Approval Centre website signifies your acceptance of such revised Terms and Conditions.'
      }
    ]
  },
  Privacy_policy_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    policy_items: [
      {
        id: 23432,
        title: 'Please read these terms and conditions of use carefully before using this web site',
        text: 'Vehicle Approval Centre may, in its sole discretion and for any reason, modify, supplement or amend these Terms and Conditions without any notice or liability to you or any other person, by posting revised Terms and Conditions on the Vehicle Approval Centre website. Your continued use of the Vehicle Approval Centre website signifies your acceptance of such revised Terms and Conditions.'
      }
    ]
  },
  Blog_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    article_items: [
      {
        id: 3423423,
        image: 'link to image',
        date: 'format YYYY-MM-DD',
        title: 'How to save on buying a car? Why is it better to buy from us'
      }
    ]
  },
  Article_page: {
    metadata: {
      title: 'Vehicle videos',
      description: 'etc',
    },
    main_content: {
      header_image: 'url to big img image',
      date: 'format YYYY-MM-DD',
      content: 'to be continue'
    }
  },
}

const Calculator_page = {
  metadata: {
    title: 'Use our free calculator!',
    description: 'etc',
  }
}

const Videos_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  videos_section: [
    {
      id: 23423,
      preview: 'url to image',
      title: 'VAC\'s Vehicle Shoot #10: 2018 Chevy Cruze Hatchback',
      y_url: 'youtube video url'
    },
  ]
}

const About_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  mission_section: {
    title: 'Our mission',
    text: 'Our mission at VAC is to help you find the perfect car at the perfect price and with the perfect auto loan. We make buying a car simple. You can complete the entire process from home—we’ll even deliver the car to you!'
  },
  dream_section: {
    title: 'Get your dream car',
    text: 'We founded VAC because we hated how complicated it was to buy a car. You only have so much free time in your life. You don’t want to waste it going to different car dealerships every weekend for weeks and months trying to track down the right car.\n We also hated how choosing a car and figuring out the financing for the vehicle were treated as totally separate processes! Instead of dealing with a salesperson, then being handed off to a closer, and then shuffled over to a finance manager, we wanted to create a way for people to buy a car and only deal with a single person for the entire process.',
    adv_items: [
      {
        id: 2343,
        title: 'Quickly',
        text: 'You do not spend a lot of your time going to car dealerships'
      },
      {
        id: 2343,
        title: 'Simply',
        text: 'You can order your dream car just by talking to our specialist by phone'
      },
      {
        id: 2343,
        title: 'Conveniently',
        text: 'We will deliver the car directly to the doorstep of your home or office'
      },
    ]
  },
  adv_section: {
    title: 'VAC is convenience and transparency',
    text: 'We want to make simple for you to find the car you want. We also want to make sure you are 100% comfortable with the terms of the conditions of your auto loan.\n Buying a car has never been easier. You pick out the car you want on our website. You work with one of our Qualified Agents to secure financing. Then we bring the car to you—all before you sign anything. You test drive the vehicle, and if you like it, you sign the loan documents right there and the car is yours.'
  },
  adv_best_choice_section: {
    title: 'Get a car at the best prices and conditions',
    content: 'Not only is buying a car with VAC easy, it’s also the best way to save money. We have partnerships with Canada’s largest lenders. That allows us to help you secure loans at the best interest rates available.\n Because convenience and transparency are our founding principles, we take the time to answer all of your questions about the vehicle and the auto loan. We will send you pictures and videos of any part of the car you like before you request a test drive.',
    subtitle: 'We think that VAC is the best way to buy a car in Canada. Try it for yourself and see how much time and money you can save.',
    sub_content: 'There are never any hidden fees or costs. We never ask you to sign anything until we have explained it clearly and you have had a chance to read it.',
  },
  agents_section: {
    title: 'Our qualified agents',
    text: 'The true secret to success of VAC isn’t our streamlined purchasing and financing processes. The real reason VAC is the easiest and best way to buy a car in Canada is because of our outstanding Qualified Agents. \n Our team provides you with the best customer experience in the automotive industry. You work with a dedicated representative throughout the process. They are able to answer any questions you have about any of our vehicles or the financing. Are you interested in seeing pictures or videos of a car? Your qualified agent will send them to you.',
    subtitle: 'Contact us today and speak with one of our qualified agents'
  },
  dynamic_team_section: {
    title: 'We\'re dynamic team of creative people with innovative mind',
    content_items: [
      {
        id: 324234,
        title: 'Our History',
        text: 'We founded VAC because we hated how complicated it was to buy a car. You only have so much free time in your life. You don’t want to waste it going to different car dealerships every weekend for weeks and months trying to track down the right car.'
      },
      {
        id: 45435,
        title: 'Our Goal',
        text: 'Buying a car has never been easier. You pick out the car you want on our website. You work with one of our Qualified Agents to secure financing. Then we bring the car to you—all before you sign anything. You test drive the vehicle, and if you like it, you sign the loan documents right there and the car is yours. '
      }
    ]
  },
  features_section: {
    title: 'Main features',
    text: 'We are your one stop shop. We believe that vehicle shopping should be a fun and painless process and with years of experience, we make it just that.',
    adv_blocks: [
      {
        id: 23423,
        title: '7,988+',
        text: 'Sold Cars'
      },
      {
        id: 23423,
        title: '123',
        text: 'Working Experts'
      },
      {
        id: 23423,
        title: '1560+',
        text: 'Happy Clients'
      },
    ]
  }
}

const Loan_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  credit_policy_section: {
    title: 'Our credit policy',
    text: 'You want to secure a loan that works for your budget. Here at VAC we work with Canada’s largest lenders to help you find the best interest rates and terms for your auto loan.',
  },
  interested_credit_rates_section: {
    title: 'Interest rates and loan terms',
    interested_items: [
      {
        id: 3242323,
        icon: 'url to svg icon',
        title: 'What are my interest rate options?',
        text: 'We work to secure customized auto loans from Canada’s largest lenders. Your interest rate is critical for determining the total cost of your auto financing. Interest rates are based on many factors including the loan amount, your credit score, and your budget. We help you find the lowest interest rates available for your auto loan.',
      },
      {
        id: 3242323,
        icon: 'url to svg icon',
        title: 'How long will my loan  term be?',
        text: 'Our goal is to find the right car and the right loan for you. Because we have relationships with so many different lenders, we can help you find the right term length for your budget. The longer the loan is, often the lower the monthly payments will be—but the more expensive the loan will be overall. Most of the loans we secure for our clients are between 36 and 72 months.'
      },
    ]
  },
  adv_loan_section: {
    title: 'Benefits of getting an auto loan with VAC',
    text: 'For many people getting a reasonable auto loan is the best way for them to get a new car. However, the benefits of getting the right loan go beyond just being able to drive a nice vehicle.\n',
    bold_text: 'When you work with VAC, there are never any hidden costs or fees! We lay out all the terms and conditions so that you know exactly what details of the loan are before you sign anything.'
  },

}

const Terms_conditions_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  terms_items: [
    {
      id: 23432,
      title: 'Please read these terms and conditions of use carefully before using this web site',
      text: 'Vehicle Approval Centre may, in its sole discretion and for any reason, modify, supplement or amend these Terms and Conditions without any notice or liability to you or any other person, by posting revised Terms and Conditions on the Vehicle Approval Centre website. Your continued use of the Vehicle Approval Centre website signifies your acceptance of such revised Terms and Conditions.'
    }
  ]
}

const Privacy_policy_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  policy_items: [
    {
      id: 23432,
      title: 'Please read these terms and conditions of use carefully before using this web site',
      text: 'Vehicle Approval Centre may, in its sole discretion and for any reason, modify, supplement or amend these Terms and Conditions without any notice or liability to you or any other person, by posting revised Terms and Conditions on the Vehicle Approval Centre website. Your continued use of the Vehicle Approval Centre website signifies your acceptance of such revised Terms and Conditions.'
    }
  ]
}

const Blog_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  article_items: [
    {
      id: 3423423,
      image: 'link to image',
      date: 'format YYYY-MM-DD',
      title: 'How to save on buying a car? Why is it better to buy from us'
    }
  ]
}

const Article_page = {
  metadata: {
    title: 'Vehicle videos',
    description: 'etc',
  },
  main_content: {
    header_image: 'url to big img image',
    date: 'format YYYY-MM-DD',
    content: 'to be continue'
  }
}

const init_data = {
  // Все марки авто
  brands: [],
  /*
    В интерфейсе есть специфическое поле поиска
    в котором по ключевому слову ищутся все модели и марки авто
  */
  all_models: [],
  // Все типы кузова
  body_types: [
    'Trucks',
    'SUV',
    'Sedan',
    'Hatchback',
    'Coupe',
    'Convertiable',
    'VAN',
  ],
  /*
    При первом заходе показываются рекомендованные авто
    (в админ панели должно быть поле как checkbox
    с помощью которого админ отмечает рекомендованны это автомобиль или нет)
    =====
    Если нет рекомендованных автомобилей показываются последние
    =====
    И так с каждыми запросами сначала в масиве должны показываться рекомендованные
    авто потом остальные. Если рекомендованных авто нет то показываются последние
   */
  cars: []
};

const get_models = {
  /*
    Все модели авто
    мне надо будет их получать когда человек выберет марку авто
 */
  models: []
}
  /*
    Параметры могут быть выбраны все или несколько
 */
const test_request_params = {
  brand: String,
  model: String,
  body_type: String,
  transmission_type: String,
  price: {
    from: [String, Number], // String or number
    to: [String, Number], // String or number
  },
  year: {
    from: [String, Number], // String or number
    to: [String, Number], // String or number
  },
  kilometers: Number
}

const car_card = {
  id: Number,
  /*
    Масив из 4 абсолютных ссылок изображений для картчки машины
    которые выбрал админ
 */
  images: [],
  /*
    Название и модель машины (в общеи говоря тайтл который пропишет админ)
 */
  title: String,
  /*
    Цена автомобиля
 */
  price: Number,
  /*
    Год выпуска
 */
  year: 'Format YYYY-MM-DD',
  /*
    Тип кузова
  */
  body_type: String,
  /*
  * Тип трансмисии
  * */
  transmission_type: String,
  /*
  * Пробег
  * */
  kilometers: Number
}

const request_one_car = {
  id: Number,
  /*
  * Название и модель машины (в общеи говоря тайтл который пропишет админ)
  * */
  title: String,
  /*
  * Настройки для 360 изображений
  * */
  three_sixty: {
    // Абсолютная ссылка на риректорию со всеми изображениями
    directory: String,
    /*
    * Все иимена у изображений должны быть в формате 'slug_(image_number)'
    * отсчет начинается с 0
    * по итогу должно получиться что то типа:
    * '2304982sdfsdk098_1.jpg'
    * */
    slug: String,
    /*
    * Количесвто изображений
    * */
    total_images_count: Number,
  },
  /*
  * Изображения для слайдера (абсолютные ссылки)
  * */
  slider_images: [],
  /*
  * Цена (масив показан просто для примера чтобы ты понимал что тип не важен)
  * */
  price: [Number, String],
  /*
  * Тип кузова
  * */
  body_type: String,
  /*
  * Год
  * */
  year: 'Format YYYY-MM-DD',
  /*
  * Трансмисия
  * */
  transmission_type: String,
  /*
  * Пробег (масив показан просто для примера чтобы ты понимал что тип не важен)
  * */
  kilometers: [String,Number],
  /*
  * Больше инфы
  * тут может быть неограниченное количесво полей
  * */
  details: {
    /*
    * Пример
    * https://www.dropbox.com/s/9qnce50954oo12o/%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202020-06-17%2003.35.27.png?dl=0
    * */
    'Ключ это название спецефической детали': 'значение соответственно контент этой детали (макс 25 символов)'
  },
  /*
  * Описане макс 750 символов
  * */
  description: String
}
