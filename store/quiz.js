import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';

const HOST = process.env.NUXT_ENV_API_HOST + '/api/';

const STATE = immutableMap({
  date: Date.now(),
  current_block: 'budget',
  car: null,
  quiz: {
    budget: null,
    status: null,
    is_working: null,
    how_earn_income: null,
    tell_about_employment: {
      Employer: null,
      Title: null
    },
    monthly_income: null,
    how_long_earning_income: {
      Years: null,
      Months: null
    },
    address: {
      'Street address': null,
      City: null,
      Province: null,
      'Postal code': null
    },
    date_born: {
      Year: null,
      Month: null,
      Day: null
    },
    main_info: {
      'First name': null,
      'Last name': null,
      Email: null,
      'Phone number': null
    },
    success: null,
  },
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
  updAbout(state, data) {
    state.quiz.tell_about_employment = data
  }
};

export const actions = {
  async send_to_CRM({ commit }, {data, other_data}) {
    commit('updateField', {path: 'done', value: true});
    try {
      const CRM = await this.$axios.post('https://cors-anywhere.herokuapp.com/https://leads.vehicleapprovalcentre.ca/tradingapi/api/DirectPost/PostRequest', data, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        }
      });
      const Other_CRM = await this.$axios.post(`https://cors-anywhere.herokuapp.com/https://leads.leadexec.net/processor/insert/general/json?${other_data}`, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        }
      });
      console.log(Other_CRM)
      // if (CRM.data.success) {
      //   commit('updateField', {path: 'current_block', value: 'success'})
      // } else {
      //   const Back = await this.$axios.post(HOST + 'crm_send', 'params_str=' + data, {
      //     headers: {
      //       'Content-Type': 'application/x-www-form-urlencoded',
      //     }
      //   })
      //   if(Back.data.success) commit('updateField', {path: 'current_block', value: 'success'})
      // }
      commit('updateField', {path: 'current_block', value: 'success'})

    } catch (e) {
      console.log(e)
    }
  },
  upd_current_block({ commit }, value) {
    commit('updateField', {path: 'current_block', value: value})
  },
  add_car({ commit }, car) {
    commit('updateField', {path: 'car', value: car})
  }
};
