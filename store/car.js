import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';

const HOST = process.env.NUXT_ENV_API_HOST + '/api/';

const STATE = immutableMap({
  date: Date.now(),
  car: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  async get_car({ commit }, id) {
    console.log(id)
    try {
      // 'https://cors-anywhere.herokuapp.com/' +
      const car = await this.$axios.get( HOST + 'get_car/?id=' + id);
      commit('updateField', { path: 'car', value: car.data })
      return true
    }
    catch (e) {
      return false
    }
  }
}
