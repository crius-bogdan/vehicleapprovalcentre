import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
import googleLink from '~/utils/googleLink';

const STATE = immutableMap({
  date: Date.now(),
  done: false,
  google_token: 'AKfycbwZduZ1MD56OTXxUFFTlHQCjbhrDq32wSHM60HS',
  form: {
    car: null,
    name: null,
    phone: null,
    email: null,
  },
});
// https://script.google.com/macros/s/AKfycbwFcXGOEdsRe9jdIWzgxZ9pa0273kZbj-4ulnbutLcxBRSwPgo/exec
// https://script.google.com/macros/s/AKfycbwFcXGOEdsRe9jdIWzgxZ9pa0273kZbj-4ulnbutLcxBRSwPgo/exec
// AKfycbwZduZ1MD56OTXxUFFTlHQCjbhrDq32wSHM60HS
// const google_link = (token) => {
//   return `https://cors-anywhere.herokuapp.com/https://script.google.com/macros/s/${token}/exec`
// }
//
export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  async send_to_mail(context, data) {
    try {
      // https://script.google.com/macros/s/AKfycbwFcXGOEdsRe9jdIWzgxZ9pa0273kZbj-4ulnbutLcxBRSwPgo/exec
      const EmailResponse = await this.$axios.post(googleLink('AKfycbwFcXGOEdsRe9jdIWzgxZ9pa0273kZbj-4ulnbutLcxBRSwPgo'), data, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      context.commit('updateField', {path: 'done', value: true});
    } catch (e) {

    }


  },
};
