import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
import sleep from '~/utils/sleep';
// import { createProxyMiddleware } from 'http-proxy-middleware';
// const test = createProxyMiddleware('/api', { target: process.env.NUXT_ENV_API_HOST + '/api/' });
// const HOST = 'https://thingproxy.freeboard.io/fetch/' + process.env.NUXT_ENV_API_HOST;
// const HOST = 'https://cors-anywhere.herokuapp.com/' + process.env.NUXT_ENV_API_HOST;
const HOST = process.env.NUXT_ENV_API_HOST + '/api/';
// const HOST = '/api/';

const STATE = immutableMap({
  date: Date.now(),
  pages: {},
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  async load(context, page) {
    context.commit('updateField', {path: 'page', value: page});
    context.commit('loading/setStatus', {name: 'page', value: true}, {root: true});
    try {
      const data = await this.$axios.$get(HOST + 'get_page?name=' + page, {
        // headers: {
        //   'GET': '/api/' + 'get_page?name=' + page,
        // }
      });
      context.commit('updateField', {path: `pages.${page}`, value: data});
    } catch (error) {
    }
    await sleep(1500);
    context.commit('loading/setStatus', {name: 'page', value: false}, {root: true});
  },
};
