import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';

const HOST = process.env.NUXT_ENV_API_HOST + '/api/';

const STATE = immutableMap({
  date: Date.now(),
  cars: null,
  last_query_string: null,
  is_loading: false,
  show_inventory: null,
  makes_list: null,
  model_list: null,
  main_search: null,
  query_error: false,
  current_page: null,
  last_page: null,
  pagination_loading: false,
  filters: {
    can_send_request: true,
    make: null,
    model: null,
    body: [],
    transmission: [],
    price_from: 0,
    price_to: 0,
    year_from: 2005,
    year_to: 2005,
    kilometres: 0
  }
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
  clear_filters(state) {
    state.filters = {
      can_send_request: true,
      make: null,
      model: null,
      body: [],
      transmission: [],
      price_from: 0,
      price_to: 0,
      year_from: 2005,
      year_to: 2005,
      kilometres: 0
    };
    state.main_search = null
  },
  add_body_type(state, data) {
    state.filters.body.push(data)
  },
  remove_body_type(state, index) {
    state.filters.body.splice(index, 1)
  },
  add_transmission(state, data) {
    state.filters.transmission.push(data)
  },
  remove_transmission(state, index) {
    state.filters.transmission.splice(index, 1)
  },
  update_cars(state, data) {
    state.cars = [...state.cars, ...data]
  }
};

export const actions = {
  async check_ip({ commit }) {
    try {
      const is_passed = await this.$axios.get(HOST + 'check_ip');
      commit('updateField', { path: 'show_inventory', value: is_passed.data.show_inventory })
      return is_passed.data.show_inventory
    } catch (e) {
      return false
    }
  },
  async get_first_data({ commit }, query) {
    try {
      const get_makes = await this.$axios.get(HOST + 'get_makes');
      const get_models = await this.$axios.get(HOST + 'get_models');
      const get_init_cars = await this.$axios.get(HOST + 'get_cars' + '?' + query);

      commit('updateField', { path: 'makes_list', value: get_makes.data })
      commit('updateField', { path: 'model_list', value: get_models.data })
      commit('updateField', { path: 'cars', value: get_init_cars.data.data })
      // Pagination
      commit('updateField', { path: 'current_page', value: get_init_cars.data.current_page })
      commit('updateField', { path: 'last_page', value: get_init_cars.data.last_page })

      return true
    } catch (e) {
      return false
    }
  },
  async query_to_get_cars({ commit }, query) {
    commit('updateField', { path: 'is_loading', value: true })
    try {
      const get_cars = await this.$axios.get(HOST + 'get_cars' + query);

      commit('updateField', { path: 'cars', value: get_cars.data.data })
      // UPD Pagination
      // console.log(get_cars.data.current_page, 'pagination current')
      // console.log(get_cars.data.last_page, 'pagination last')
      commit('updateField', { path: 'current_page', value: get_cars.data.current_page })
      commit('updateField', { path: 'last_page', value: get_cars.data.last_page })
      // Save last query
      commit('updateField', { path: 'last_query_string', value: query })

      commit('updateField', { path: 'is_loading', value: false })
    } catch (e) {
      commit('updateField', { path: 'cars', value: [] })
    }
  },
  async get_pagination_data({ commit, state: { last_query_string } }, page) {
    commit('updateField', { path: 'pagination_loading', value: true })
    try {
      let query_string = !!last_query_string ?  last_query_string + '&page=' + page : `?page=${page}`;

      const get_cars = await this.$axios.get(HOST + 'get_cars' + query_string);
      commit('update_cars', get_cars.data.data)
      // UPD Pagination
      commit('updateField', { path: 'current_page', value: get_cars.data.current_page })
      commit('updateField', { path: 'last_page', value: get_cars.data.last_page })

      commit('updateField', { path: 'pagination_loading', value: false })
    } catch (e) {

    }
  }
}
