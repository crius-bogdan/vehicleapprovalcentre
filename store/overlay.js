import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';

const STATE = immutableMap({
  date: Date.now(),
  overlay: null,
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  setContactForm(context, value) {
    context.commit('updateField', {path: 'overlay', value: 'OverlayContactForm'});

    if (value) {
      context.commit('contact-form/updateField', {path: 'form.car', value}, {root: true});
    }
  }
};
