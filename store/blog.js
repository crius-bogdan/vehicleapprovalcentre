import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
import sleep from '~/utils/sleep';

// const HOST = 'https://thingproxy.freeboard.io/fetch/' + process.env.NUXT_ENV_API_HOST;
// const HOST = process.env.NUXT_ENV_API_HOST;
// const HOST = '/api/';
const HOST = process.env.NUXT_ENV_API_HOST + '/api/';
const STATE = immutableMap({
  date: Date.now(),
  posts: [],
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  async load(context, {num, id}) {
    context.commit('loading/setStatus', {name: 'blog', value: true}, {root: true});
    const query = {};
    if (num) {
      query.num = num;
    }
    if (id) {
      query.id = id;
    }
    try {
      const data = await this.$axios.$get(HOST + 'get_blog_posts', {query});
      context.commit('updateField', {path: 'posts', value: data});
    } catch (error) {
    }
    await sleep(1500);
    context.commit('loading/setStatus', {name: 'blog', value: false}, {root: true});
  },
};
