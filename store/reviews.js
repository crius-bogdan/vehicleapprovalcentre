import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
import sleep from '~/utils/sleep';

// const HOST = 'https://thingproxy.freeboard.io/fetch/' + process.env.NUXT_ENV_API_HOST;
const HOST = process.env.NUXT_ENV_API_HOST + '/api/';
// const HOST = '/api/';
const STATE = immutableMap({
  date: Date.now(),
  reviews: [],
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  async load(context) {
    context.commit('loading/setStatus', {name: 'reviews', value: true}, {root: true});
    try {
      const data = await this.$axios.$get(HOST + 'get_reviews');
      context.commit('updateField', {path: 'reviews', value: data});
    } catch (error) {
    }
    await sleep(1500);
    context.commit('loading/setStatus', {name: 'reviews', value: false}, {root: true});
  },
};
