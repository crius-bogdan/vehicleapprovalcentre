export default {
  props: {
    checkboxes: Array,
    value: Array
  },
  methods: {
    change_val(value) {
      this.$emit("input", value)
    }
  },
}
