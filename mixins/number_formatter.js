export default {
  filters: {
    number_format(value) {
      return new Intl.NumberFormat('en-US').format(value)
    }
  }
}
