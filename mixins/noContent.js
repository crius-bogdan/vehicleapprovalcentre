export default {
  computed: {
    noContent() {
      if (!this.content) {
        return true;
      }
      for (const i in this.content) {
        return false;
      }
      return true;
    },
  },
};
