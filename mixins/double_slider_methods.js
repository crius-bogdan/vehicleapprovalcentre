export  default {
  props: {
    value: Object,
    clear_filters: Boolean,
  },
  data() {
    return {
      change_val: null,
      tags: [],
    }
  },
  computed: {
    slider_val: {
      get() {
        return [this.value.from, this.value.to];
      },
      set(value) {
        this.$emit('input', { val_from: value[0], val_to: value[1], can_send: false })
        this.change_val = value;
      },
    },
    dot_size() {
      return window.innerWidth < 1100
    }
  },
  watch: {
    clear_filters(val) {
      if(val) this.tags = []
    }
  },
  methods: {
    end_change() {
      this.$emit('input', { val_from: this.change_val[0], val_to: this.change_val[1], can_send: true })
      if(this.is_price || this.is_year) {
        if(this.change_val[1] === 0 || this.change_val[1] === 2005) {
          this.tags = []
          return
        }
      }
      const formatted_number_first = new Intl.NumberFormat('en-US').format(this.change_val[0]);
      const formatted_number_second = new Intl.NumberFormat('en-US').format(this.change_val[1]);
      const dollar = this.is_price ? '$' : '';
      this.tags = [`${dollar} ${this.is_price ? formatted_number_first : this.change_val[0]} - ${this.is_price ? formatted_number_second : this.change_val[1]}`]
    },
    clear_tags(value) {
      this.$emit('input', value)
      this.tags = []
    }
  },
}
