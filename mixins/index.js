export const quiz_method = {
  props: {
    value: String
  },
  data() {
    return {
      validation_error: {}
    }
  },
  computed: {
    is_valid() {
      return Object.values(this.validation_error).find(e => !!e)
    },
  },
  created() {
    const check_is_valid = Object.keys(this.validation_error).length > 0;
    const check_value = (!this.value || this.value.length === 0);
    check_is_valid ? this.$emit('validation', true) :
    check_value ? this.$emit('validation', true) :
    this.$emit('validation', false);
  },
  methods: {
    changeValue(val) {
      const check_valid_errors = Object.values(this.validation_error).length > 0;
      const resolve_condition = check_valid_errors ? this.is_valid : val.length === 0;
      this.$emit('input', val);
      this.$emit('validation', resolve_condition);
    },
  }
}
export const quiz_multiple_input = {
  props: {
    value: Object
  },
  data() {
    return {
      val: {},
      validation_error: {}
    }
  },
  computed: {
    fields() {
      return Object.keys(this.value)
    },
    is_valid() {
      return Object.values(this.validation_error).find(e => !!e);
    },
  },
  mounted() {
    !!this.is_valid ? this.$emit('validation', true) :
      this.$emit('validation', false);
    if(this.is_address) {
      const fields = [
          { element: "address-street-address", field: "Line1", mode: pca.fieldMode.DEFAULT },
          { element: "address-city", field: "City", mode: pca.fieldMode.POPULATE },
          { element: "address-province", field: "Province", mode: pca.fieldMode.POPULATE },
          { element: "address-postal-code", field: "PostalCode" },
        ],
        options = {
          key: "yc78-hk92-yz91-ef72"
        },
        control = new pca.Address(fields, options);
        control.listen("populate", ({ FormattedLine1, City, Province, PostalCode }) => {
          this.changeValue({ name: 'Street address', value: FormattedLine1 });
          this.changeValue({ name: 'City', value: City });
          this.changeValue({ name: 'Province', value: Province });
          this.changeValue({ name: 'Postal code', value: PostalCode }, true);
        });
    }
  },
  updated() {
  },
  methods: {
    changeValue({ name, value }, is_address_complete_ready) {
      if(is_address_complete_ready) {
        this.$emit('input', { name, value, valid: undefined });
        this.$emit('validation', undefined)
      } else {
        this.$emit('input', { name, value, valid: !!this.is_valid });
        this.$emit('validation', this.is_valid)
      }
    },
  },
}
export const input_mixin = {
  props: {
    inputClass: String,
    placeholder: {
      type: String,
    },
    value: {
      type: [String, Number],
      default: '',
    },
    is_icon: {
      type: Boolean,
    },
    input_name: String,
    type: String,
    check_empty: Boolean,
    check_email: Boolean,
    check_name: Boolean,
    check_month: Boolean,
    is_quiz: Boolean,
    quiz_name: String,
    only_numbers: Boolean,
    check_year: Boolean,
    check_day: Boolean,
    check_born_month: Boolean,
    is_address: Boolean,
    input_id: String,
  },
  data() {
    return {
      val: this.value,
      id: null,
      is_focus: false,
      is_error: false,
      show_validation: false,
      default_mask: '+1 (___)___-____'
    };
  },
  computed: {
    error() {
      if (!this.val || this.val.length === 0) return 'empty';

      if(this.check_work_year && +this.val > 70) {
        return 'Bad work year'
      }

      if(this.is_phone_input && (!this.val || this.val.includes('_') ) ) {
        return 'Not a phone number';
      } else if(this.is_phone_input) {
        return false;
      }

      if(this.check_year) {
        const date_now = new Date();
        const year_now = date_now.getFullYear();
        const good_lead_year = year_now - 18;
        if(!this.val || +this.val > good_lead_year || +this.val < 1920) {
          return 'Bad Year'
        }
      }

      if(this.check_day && (!this.val || +this.val > 31)) {
        return 'Bad day'
      }

      if(this.check_born_month && (!this.val || +this.val > 12)) {
        return 'Higher than 12'
      }

      if(this.check_month && (+this.val > 11)) {
        return 'Higher than 11';
      }

      if (this.check_email && !(
        /\S+@\S+\.\S+/.test(this.val)
      )) {
        return 'not-email';
      }

      // if (this.check_name && !(
      //   /^[a-z0-9]+(?:-[a-z0-9]+)*$/.test(this.val)
      // )) {
      //   return 'not-name';
      // }

      return false;
    },
    visible_error() {
      return this.show_validation ? this.error : false;
    },
  },
  created() {
    this.emit_validation();
  },
  mounted() {
    this.id = 'this._uid'; // TODO: Fix `_uid` if id is needed.
    // if(this.is_address) {
    //   setInterval(() => {
    //     this.change();
    //   }, 1000)
    // }
  },
  updated() {

  },
  methods: {
    change() {
      if(this.check_email) this.val = this.val.replace(/[\u0430-\u044f\u0451]/ig, '')
      if(!this.is_phone_input) this.emit_validation();
      if (this.is_phone_input) this.phone_mask();
      if(this.only_numbers) this.val = this.val.replace(/[^\d]/g, '')
      if(this.is_quiz && this.quiz_name) {
        this.$emit('input', {name: this.quiz_name, value: this.val});
      } else {
        this.$emit('input', this.val);
      }
    },
    blur_input() {
      this.is_focus = false;
      this.show_validation = true;
    },
    focus_input() {
      if(this.is_phone_input) {
        this.val = (!this.val || this.val.length === 0) > 0 ? this.default_mask : this.val;
        this.set_cursor_position(4);
      }
      this.is_focus = true;
    },
    emit_validation() {
      this.$emit('update:validation_error', this.error);
    },
    phone_mask() {
      let matrix = this.default_mask,
          i = 0,
          def = matrix.replace(/\D/g, ""),
          val = this.val.replace(/\D/g, "");
          def.length >= val.length && (val = def);

      matrix = matrix.replace(/[_\d]/g, (a) => {
        return val.charAt(i++) || "_"
      });
      this.val = matrix;
      i = matrix.lastIndexOf(val.substr(-1));
      i < matrix.length && matrix !== this.default_mask ? i++ : i = matrix.indexOf("_");
      this.$nextTick(() => this.set_cursor_position(i));
      this.emit_validation();
    },
    set_cursor_position(pos) {
      const elem = this.$refs.input;
      elem.focus()
      elem.setSelectionRange(pos, pos)
    }
  },
}
