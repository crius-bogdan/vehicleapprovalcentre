export default {
  props: {
    current_block: String
  },
  methods: {
    change_current_block(name) {
      this.$emit('change-block', name)
    }
  },
}
